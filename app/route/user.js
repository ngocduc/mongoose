import express from 'express';
import User from '../Model/user';
import Comment from '../Model/comment';

const router = express.Router();

router.get('/create/:name', async (req, res) => {
	try {
		const newUser = new User({
			userName: req.params.name,
			password: '1234',
		})
		const result = await newUser.save();
		res.json(result);

	} catch (err) {
		res.json(err);
	}
});

router.get('/addComment/:name', async (req, res) => {
	try {
	const user = await User.findOne({userName: new RegExp(req.params.name, 'i') });
	const newComment = new Comment({
		title: 'String',
		body: 'String',
		author: user._id,
	});
	const commentSaved = await newComment.save();
	const re = await User.updateOne({ _id: user._id }, { $push: { comment: commentSaved._id } })

	res.json(re);

	} catch (err) {
		res.json(err);
	}
});

router.get('/:name', async (req, res) => {
	try {
	const user = await User
		.findOne({userName: new RegExp(req.params.name, 'i') })
		.populate('comment')
		;
	res.json(user);

	} catch (err) {
		res.json(err);
	}
});

router.get('dnd/:dnd([0-9a-f]{2})', (req, res) => {
	console.log('req.params', req.params);
  const { dnd = 'dnd' } = req.params;
	console.log('dnd', dnd);
	res.send(dnd);
});

router.get('dnd', (req, res) => {
	console.log('1111111111111111111111111111' );
	// console.log('req.params', req.params);
  // const { dnd = 'dnd' } = req.params;
	// console.log('dnd', dnd);
	// res.send(dnd);
});

export default router;