import express from 'express';
import userRoute from './user';
// test==========
import { validaTest } from '../Validate';
import { schemaTest } from '../Validate/schema';

// import nodemailer from 'nodemailer';

const router = express.Router();

// middleware that is specific to this router
router.use('/user', userRoute);

// ============================================================================ test
// router.get('/', () => {
// });
// ============================================================================ test
try {
	const data = {
		foo: 1,
		bar: 2
	};

	const re = validaTest(schemaTest, data);
	console.log('re', re);
} catch(err) {
	console.log('err', err);
}
export default router;