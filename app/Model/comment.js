import mongoose, { Schema } from 'mongoose';

const commentSchema = new Schema({
  title: String,
  body: String,
	author: { type: Schema.Types.ObjectId, ref: 'User' },
});

const User = mongoose.model('Comment', commentSchema);


commentSchema.methods.getAuthor = function () {
  // console.log('this.name', this.title);
}
export default User;