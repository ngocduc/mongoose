import mongoose from 'mongoose';

const Any = new mongoose.Schema({ any: mongoose.Mixed });
// Note that if you're using `type`, putting _any_ POJO as the `type` will
// make the path mixed.
// const Any = new Schema({
//   any: {
//     type: { foo: String }
//   }
// });
// export const AnyModel = mongoose.model('Any', Any);
// ========================================================================================
// const userSchema = new mongoose.Schema({
//   // `socialMediaHandles` is a map whose values are strings. A map's
//   // keys are always strings. You specify the type of values using `of`.
//   socialMediaHandles: {
//     type: Map,
//     of: String
//   }
// });

// const User = mongoose.model('Any', userSchema);
// Map { 'github' => 'vkarpov15', 'twitter' => '@code_barbarian' }
// console.log(new User({
//   socialMediaHandles: {
//     github: 'vkarpov15',
//     twitter: '@code_barbarian'
//   }
// }).socialMediaHandles);
// ========================================================================================  var animalSchema = new Schema({ name: String, type: String });
const userSchema = new mongoose.Schema({
  userName: String,
  password: String,
  type: String,
});

userSchema.methods.findSimilarTypes = function(cb) {
  return this.model('User').find({ type: this.type }, cb);
};
var Animal = mongoose.model('User', userSchema);
var ducdn = new Animal({
  type: 'dnd',
  userName: 'ducdn1',
  password: 'String',
});
ducdn.save();

ducdn.findSimilarTypes(function(err, dogs) {
  console.log(1111111111111111111, new mongoose.Types.ObjectId()); // woof
});
// export default User;
