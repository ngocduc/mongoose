import mongoose, { Schema } from 'mongoose';

const userSchema = new Schema({
  userName: { type: String, required: true },
  password: String,
	comment: [ { type: Schema.Types.ObjectId, ref: 'Comment' }]
});

userSchema.pre('find', next => {
  console.log('ddddd', this );
  next();
})

userSchema.methods.getComment = function () {
  // console.log('this.name', this.userName);
}
userSchema.methods.findUserByName = function () {
  console.log('this.userName', this.userName);
  return this.model('User').find({userName: new RegExp(this.userName, 'i') });
}

const User = mongoose.model('User', userSchema);
export default User;
// // =====================================

//   // assign a function to the "methods" object of our animalSchema
// userSchema.methods.findSimilarTypes = function(cb) {
//   return this.model('User').find({ type: this.type }, cb);
// };
// var Animal = mongoose.model('User', userSchema);
// var ducdn = new Animal({
//   type: 'dnd',
//   userName: 'ducdn1',
//   password: 'String',
// });
// ducdn.save();

// ducdn.findSimilarTypes(function(err, dogs) {
//   console.log(dogs); // woof
// });
// // export default User;
