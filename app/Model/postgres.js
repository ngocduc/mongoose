const Sequelize = require('sequelize');

// Option 1: Passing parameters separately
const sequelize = new Sequelize('dnd', 'postgres', 'dnd', {
  host: 'localhost',
  dialect: 'postgres'/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});


const Model = Sequelize.Model;
class Member extends Model {}
Member.init({
  // attributes
  firstName: {
    type: Sequelize.STRING,
    allowNull: false
  },
  lastName: {
    type: Sequelize.STRING
    // allowNull defaults to true
  }
}, {
  sequelize,
  modelName: 'member'
  // options
});
sequelize.sync()
  .then(() => Member.create({
    firstName: 'ngoc',
    lastName: 'duc',
  }))
  .then(dnd => {
    console.log(dnd.toJSON());
  });