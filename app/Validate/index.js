import Ajv from 'ajv';
import ajvErros from 'ajv-errors';
let ajv = new Ajv({allErrors: true, jsonPointers: true});
ajvErros(ajv)
// Ajv options allErrors and jsonPointers are required
// require('ajv-errors')(ajv /*, {singleError: true} */);
const validaTest = (schema, data) => {
	var validate = ajv.compile(schema);
	const result = validate(data); // false
	if(!result) throw validate.errors; // processed errors
	return result;
}
export {
	validaTest
}