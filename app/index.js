import express from 'express';
import mongoose from 'mongoose';
import moment from 'moment';
import routers from './route';
import User from './Model/user';
import _ from 'lodash';

import sequelize from './Model/postgres';
import { userInfo } from 'os';

const app = express();

/*CORS middleware */
app.use(function(req, res, next) { 
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
	next();
});
app.use(nocache);
function nocache(req, res, next) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    next();
}
// config
// mongoose.connect('mongodb://ducdn:ngocduc95@ds125932.mlab.com:25932/top_gun');
const port = 5000;

// app.use('/', routers);
// app.get('/dnd/:dnd([1-9a-z]{2})', (req, res) => {
// 	console.log('11111111111211111111111111111' );
// 	console.log('req.params', req.params);
//   const { dnd = 'dnd' } = req.params;
// 	console.log('dnd', dnd);
// 	res.send(dnd);
// });
// test ===================================
// const yesterday =  moment().subtract(1, 'days');
// console.log(
// 	'ddddddddddd', moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')
// )
// test ==========================

app.get('/NameModule', (req, res) => {
	res.json([{
		"Module": "BkavCrash.exe",
		"MS": "2"
	},{
		"Module": "Engine.exe",
		"MS": "3"
	},{
		"Module": "Engine.exe",
		"MS": "3"
	},{
		"Module": "Engine.exe",
		"MS": "3"
	},{
		"Module": "Engine.exe",
		"MS": "3"
	},{
		"Module": "Engine.exe",
		"MS": "3"
	},{
		"Module": "Engine.exe",
		"MS": "3"
	},{
		"Module": "Engine.exe",
		"MS": "3"
	},{
		"Module": "Engine.exe",
		"MS": "3"
	},{
		"Module": "Engine.exe",
		"MS": "3"
	},{
		"Module": "Engine.exe",
		"MS": "3"
	},{
		"Module": "Engine.exe",
		"MS": "3"
	}]);
});
const test = async () => {
	try {
		[1,2,2].map(async i => {
				// console.log('------------------------------', i)
			const a = await User.find({_id: 232})
			return a;
			// .catch(err => {
			// 	console.log('err111111111111111111111111111111c');
			// 	return err;
			// })
				// throw err;
		});
	} catch(err) {
		console.log('1222222222222222222222222222222222222222');
		// return err;
	}
}
// test();

// console.log(moment().add(-1, 'days'));
// app.get('dnd/:dnd([0-9a-f]{24})', (req, res) => {
//   const { dnd } = req.params;
//   console.log('dnd', dnd);
// });
const dateTest = date => {
	// const newPaymentTransaction = {
	// 	transferDate: moment()
	// }
	// console.log('11111111111' );
	// console.log('moment().day()', moment().day(), moment().date());
	// switch (date) {
	// 	case 'daily':
	// 		if(moment().day() === 0) return null;
	// 		break;
	// 	case 'weekly':
	// 		newPaymentTransaction.transferDate = moment().add(3 - moment().day(), 'days');
	// 		break;
	// 	case 'twice_a_month':
	// 		if(moment().date() <= 15) newPaymentTransaction.transferDate = moment().date(14);
	// 		else newPaymentTransaction.transferDate = moment().endOf('month').subtract(1, "days");
	// 		break;
	// 	case 'monthly':
	// 		newPaymentTransaction.transferDate = moment().endOf('month').subtract(1, "days");
	// 		break;
	// }
	// console.log('newPaymentTransaction', newPaymentTransaction);

	// test
	const a = { '5': 1, '6': 2, '0': 3 }
  const dateDnd = a[moment().date(14).day()] 
	console.log('dd', moment().date(14).subtract(dateDnd, "days"));
	
}
// dateTest('monthly');

// =====================================

const autoPaymentHandle = async () => {
	const data = getThursDay(moment().date(14)).date();
	console.log('data', data);
};

function getThursDay(dateInput) {
  const date = _.cloneDeep(dateInput);
  const schemaMap = { '5': 1, '6': 2, '0': 3 };
  const subDate = schemaMap[date.day()];
  return date.subtract(subDate, "days");
}
// autoPaymentHandle();
// *************************************************************************

// app.use('/test-postgres', () => {
// 	sequelize
// 	  .authenticate()
// 	  .then(() => {
// 	    console.log('Connection has been established successfully.');
// 	  })
// 	  .catch(err => {
// 	    console.error('Unable to connect to the database:', err);
// 	  });
// });


const dnd = async () => {
	const userData = await User.find({userName: 'user'});
	console.log({userData});
}
// dnd();

// eslint-disable-next-line no-console
app.listen(port, () => console.log(`app listening on port ${port}!`));