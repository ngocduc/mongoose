export default (sequelize, dataType) => {
	const User = sequelize.define('user', {
		userName: {
			type: dataType.STRING,
			unique: true,
		},
		email: {
			type: dataType.STRING,
			unique: true,
		},
		password: dataType.STRING,
	});
	User.associate = models => {
		User.belongsToMany(models.Team, {
			through: {
				model: 'member',
				unique: false,
			},
			foreignKey: 'userId',
		    constraints: false
		})
	}
	return User;
}	