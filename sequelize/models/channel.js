export default (sequelize, dataType) => {
	const User = sequelize.define('user', {
		name: {
			type: dataType.STRING,
			unique: true,
		},
		public: {
			type: dataType.BOOLEAN,
		},
	});
	User.associate = models => {
		User.belongsTo(models.Team, {
			foreignKey: 'teamId',
		    constraints: false
		})
	}
	return User;
}	