export default (sequelize, dataType) => {
	const Messgase = sequelize.define('messgase', {
		text:  dataType.TEXT,
	});
	Messgase.associate = models => {
		Messgase.belongsTo(models.User, {
			foreignKey: 'teamId',
		    constraints: false
		})
		Messgase.belongsTo(models.Channel, {
			foreignKey: 'channelId',
		    constraints: false
		})
	}
	return Messgase;
}	