export default (sequelize, dataType) => {
	const Team = sequelize.define('team', {
		name: {
			type: dataType.STRING,
			unique: true,
		},
	});
	Team.associate = models => {
		Team.belongsToMany(models.User, {
			through: {
				model: 'member',
				unique: false,
			},
			foreignKey: 'teamId',
		    constraints: false
		})
		Team.belongsTo(models.User, {
			foreignKey: 'owner'
		})
	}
	return Team;
}	