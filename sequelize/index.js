import express from 'express';
import mongoose from 'mongoose';
import moment from 'moment';
import _ from 'lodash';

import models from './models';

const app = express();

/*CORS middleware */
app.use(function(req, res, next) { 
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
	next();
});
app.use(nocache);
function nocache(req, res, next) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    next();
}
// ------------------------------------------------------------------



// -------------------------------------------------------------------
models.sequelize.sync({}).then(() => {
	const port = 3001;
	app.listen(port, () => console.log(`app listening on port ${port}!`));
});